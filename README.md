Colobase 2002
========

This is an online database of units from Coloball 2002, a PlayStation 2 game by Vanpool. The purpose of this project is to provide an easily navigable collection of all the game's units and spells in English. I hope you're ready to become a true ProBoller.


ProBoller's Belief
==================

1. Play with integrity. Know that all actions and remarks will be seen by others and judged.

2. Practice good etiquette and control yourself. The spirit dweels within the unit and the roulette is decided by the heart.

3. We should spare no effort and always improve ourselves. Daily research and battles with rivals will create a strong Baller.

4. Do everything with the Coliseum Ball. Eat, sleep, Coloball.

5. It is also the duty of the Baller to increase the number of Ballers. Search for new rivals and promote Coliseum Ball everywhere.


How to Use
==========
This is a website app that should be able to run in most modern browsers. You will not need Flash or anything to use this. The database will be designed to emulate the layout of the in-game unit database that you can look through in the Team Menu, so finding units should be a breeze. You can read through every unit in the game and see a variety of information.

This is a work-in-progress project. For questions, comments, or problems, please raise an issue or contact Tello#1942 on Discord.





