import axios from 'axios'
import django_url_base from './django_url_base'

// these are necessary for axios to be able to POST/PUT/PATCH to our api (GET works fine without):
axios.defaults.xsrfCookieName = 'csrftoken' // matches django's CSRF_COOKIE_NAME: 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN' // matches django's CSRF_HEADER_NAME: 'HTTP_X_CSRFTOKEN'
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'
axios.defaults.headers.patch['Content-Type'] = 'application/json'

const api = axios.create({
  baseURL: django_url_base + '/api/'
})

export default {
  logged_in () {
    return api.get('logged_in/')
  },
  card_list () {
    return api.get('cards/')
  },
  spell_list () {
    return api.get('spells/')
  },
  project_list () {
    return api.get('decks/')
  },
}