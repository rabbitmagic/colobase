// this switch statement picks '' in production (when being built and served with whitenoise),
// and 'localhost:8000' in dev mode (when previewing vue changes at localhost:3000).
// use me like django_url_base + '/admin/'.
export default window.location.hostname === 'localhost' ? 'http://localhost:8000' : ''
