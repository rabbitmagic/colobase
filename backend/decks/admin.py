from decks.models import Deck
from django.contrib import admin

admin.site.register(Deck)
