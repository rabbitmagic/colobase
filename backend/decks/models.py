from django.db import models

"""
Decks
-----

Decks are a player's selection of units, land panels, and spells.

When building decks, the game has various checks to show if a deck is legal in 10-round games and 20-round games.

Decks have:

- name
- up to 20 units, in specific draw order
  - up to 10 units or 30 total rank points for half games
- exactly 10 unordered land panels, a selection of 4 colors (red, green, blue, yellow)
  - can be represented as a tuple of four ints that always sums to 10
  - e.g. (0, 5, 5, 0) represents 5 green, 5 blue
- up to 10 spells
  - up to 5 spells for a half game
  - spell energy costs must add up to no higher than your deck's panel colors.

"""


class Deck(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.TextField(blank=True)
    units = models.ManyToManyField("units.Unit")
    land_red = models.PositiveSmallIntegerField(default=0)
    land_green = models.PositiveSmallIntegerField(default=0)
    land_blue = models.PositiveSmallIntegerField(default=0)
    land_yellow = models.PositiveSmallIntegerField(default=0)
    spells = models.ManyToManyField("spells.Spell")
