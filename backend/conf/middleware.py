from django.urls import is_valid_path
from whitenoise.middleware import WhiteNoiseMiddleware


class WhiteNoiseSpaMiddleware(WhiteNoiseMiddleware):
    """
    WhiteNoise alone allows deploying single-page applications on django
    by setting WHITENOISE_INDEX and WHITENOISE_ROOT.

    However, this setup can break if the client application navigates
    (changes the url itself) when the user refreshes on the updated url.

    This wrapper for WhiteNoiseMiddleware fills this gap,
    serving the index file on any paths not found in the urlconf.
    """

    def process_request(self, request):
        # let WhiteNoiseMiddleware go first.
        super_response = super().process_request(request)
        if super_response is not None:
            return super_response
        # if requested path is not a known url pattern,
        # and hasn't yet been caught by whitenoise,
        # serve index.html for all routes within this SPA.
        # also account for possible APPEND_SLASH redirects.
        urlconf = getattr(request, "urlconf", None)
        if (
            self.index_file
            and self.root
            and not is_valid_path(request.path_info, urlconf)
            and not is_valid_path("%s/" % request.path_info, urlconf)
        ):
            file_path = "/"
            # the rest proceeds similar to WhiteNoiseMiddleware.
            if self.autorefresh:
                static_file = self.find_file(file_path)
            else:
                static_file = self.files.get(file_path)
            if static_file is not None:
                return self.serve(static_file, request)
