from django.db import models

"""
Unit
----

Units are little guys that you put out on the field. They have...

- numbers from 1 to 500 (or more?)
- rank (the star in corner. an indicator of difficulty. killing this unit yields this many points)
- energy (cost to summon, to put on the field)
  - e.g. green=1, grey=3.
  - (energy is white black red green blue yellow) 
- up to 4 moves (waza) (corresponding to face button colors)
  - circle is short range, triangle is long range attack, cross is block, and square is dodge.
  - a move has a type (button), a japanese name, an english name, a number, sometimes a condition.
- up to 4 passives (fx)
- 1 skill (japanese text, english text)
- lore (japanese text, english text)
- another passive (sfx) (japanese text, english text)
"""


class Unit(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    rank = models.PositiveSmallIntegerField(default=1)
    hp = models.PositiveSmallIntegerField(default=1)
    title_jp = models.TextField(blank=True)
    title_en = models.TextField(blank=True)
    name_jp = models.TextField(blank=True)
    name_en = models.TextField(blank=True)
    species = models.TextField(blank=True)
    energy_grey = models.PositiveSmallIntegerField(default=0)
    energy_red = models.PositiveSmallIntegerField(default=0)
    energy_green = models.PositiveSmallIntegerField(default=0)
    energy_blue = models.PositiveSmallIntegerField(default=0)
    energy_yellow = models.PositiveSmallIntegerField(default=0)
    lore_jp = models.TextField(blank=True)
    lore_en = models.TextField(blank=True)
    # circle is short range attack
    waza_short_jp = models.TextField(blank=True)
    waza_short_en = models.TextField(blank=True)
    waza_short_strength = models.TextField(blank=True)
    waza_short_accuracy = models.TextField(blank=True)
    # triangle is long range attack
    waza_long_jp = models.TextField(blank=True)
    waza_long_en = models.TextField(blank=True)
    waza_long_strength = models.TextField(blank=True)
    waza_long_accuracy = models.TextField(blank=True)
    # cross is block move
    waza_block_jp = models.TextField(blank=True)
    waza_block_en = models.TextField(blank=True)
    waza_block_strength = models.TextField(blank=True)
    waza_block_accuracy = models.TextField(blank=True)
    # square is dodge move
    waza_dodge_jp = models.TextField(blank=True)
    waza_dodge_en = models.TextField(blank=True)
    # (dodge has no strength)
    waza_dodge_accuracy = models.TextField(blank=True)
    # skills
    skill_title_jp = models.TextField(blank=True)
    skill_title_en = models.TextField(blank=True)
    skill_desc_jp = models.TextField(blank=True)
    skill_desc_en = models.TextField(blank=True)
    skill_energy_grey = models.PositiveSmallIntegerField(default=0)
    skill_energy_red = models.PositiveSmallIntegerField(default=0)
    skill_energy_green = models.PositiveSmallIntegerField(default=0)
    skill_energy_blue = models.PositiveSmallIntegerField(default=0)
    skill_energy_yellow = models.PositiveSmallIntegerField(default=0)
    # fx are modifiers on attacks and moves
    fx_short = models.TextField(blank=True)
    fx_long = models.TextField(blank=True)
    fx_block = models.TextField(blank=True)
    fx_dodge = models.TextField(blank=True)
    # sfx are modifiers on the entire unit
    sfx_title_jp = models.TextField(blank=True)
    sfx_title_en = models.TextField(blank=True)
    sfx_desc_jp = models.TextField(blank=True)
    sfx_desc_en = models.TextField(blank=True)
