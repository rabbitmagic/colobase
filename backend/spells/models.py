from django.db import models

"""
Spell
-----

each spell has...

- strength of a certain color (grey, red, green, blue, yellow)
- japanese text, english text

"""


class Spell(models.Model):
    id = models.PositiveSmallIntegerField(primary_key=True)
    strength_grey = models.PositiveSmallIntegerField(default=0)
    strength_red = models.PositiveSmallIntegerField(default=0)
    strength_green = models.PositiveSmallIntegerField(default=0)
    strength_blue = models.PositiveSmallIntegerField(default=0)
    strength_yellow = models.PositiveSmallIntegerField(default=0)
    text_jp = models.TextField(blank=True)
    text_en = models.TextField(blank=True)
